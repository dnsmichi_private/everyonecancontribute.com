# everyonecancontribute.com

This repository provides configuration and documentation
for setting up https://everyonecancontribute.com with Docker,
Nginx, Let's Encrypt and Prometheus for Monitoring.

The Docker setup with Nginx follows [this awesome tutorial](https://www.humankode.com/ssl/how-to-set-up-free-ssl-certificates-from-lets-encrypt-using-docker-and-nginx).

## Pre-requisites

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce
```

```
curl -L https://github.com/docker/compose/releases/download/1.25.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

## System

### Firewall

```
ufw allow OpenSSH
ufw allow 'Nginx Full'
systemctl start ufw
```

```
apt-get install fail2ban
systemctl enable fail2ban
systemctl start fail2ban

cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
vim /etc/fail2ban/jail.conf

banaction = ufw

fail2ban-client reload

fail2ban-client status
```

### System 

Don't phone home.

```
sed -i 's/^ENABLED=.*/ENABLED=0/' /etc/default/motd-news
```

### Upgrades

https://help.ubuntu.com/lts/serverguide/automatic-updates.html
https://unix.stackexchange.com/questions/178626/how-to-run-unattended-upgrades-not-daily-but-every-few-hours

```
vim /etc/apt/apt.conf.d/20auto-upgrades

APT::Periodic::Update-Package-Lists "always";
APT::Periodic::Download-Upgradeable-Packages "always";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "always";
```

```
systemctl edit apt-daily.timer

[Timer]
OnCalendar=
OnCalendar=*-*-* 0,4,8,12,16,20:00
RandomizedDelaySec=15m

systemctl edit apt-daily-upgrade.timer

[Timer]
OnCalendar=
OnCalendar=*-*-* 0,4,8,12,16,20:20
RandomizedDelaySec=1m


systemctl cat apt-daily{,-upgrade}.timer
systemctl --all list-timers apt-daily{,-upgrade}.timer
```



## Let's Encrypt Container

```
mkdir -p /docker/letsencrypt-docker-nginx/src/letsencrypt/letsencrypt-site
vim /docker/letsencrypt-docker-nginx/src/letsencrypt/docker-compose.yml

version: '3.1'

services:

  letsencrypt-nginx-container:
    container_name: 'letsencrypt-nginx-container'
    image: nginx:latest
    ports:
      - "80:80"
    volumes:
      - ./nginx.conf:/etc/nginx/conf.d/default.conf
      - ./letsencrypt-site:/usr/share/nginx/html
    networks:
      - docker-network

networks:
  docker-network:
    driver: bridge
```

```
vim /docker/letsencrypt-docker-nginx/src/letsencrypt/nginx.conf

server {
    listen 80;
    listen [::]:80;
    server_name everyonecancontribute.com www.everyonecancontribute.com;

    location ~ /.well-known/acme-challenge {
        allow all;
        root /usr/share/nginx/html;
    }

    root /usr/share/nginx/html;
    index index.html;
}
```

```
vim /docker/letsencrypt-docker-nginx/src/letsencrypt/letsencrypt-site/index.html

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Let's Encrypt First Time Cert Issue Site</title>
</head>
<body>
    <h1>Oh, hai there!</h1>
    <p>
        This is the temporary site that will only be used for the very first time SSL certificates are issued by Let's Encrypt's
        certbot.
    </p>
</body>
</html>
```

### Run Container
```
cd /docker/letsencrypt-docker-nginx/src/letsencrypt
docker-compose up -d
```

## Certificate

### Staging certificate

```
sudo docker run -it --rm \
-v /docker-volumes/etc/letsencrypt:/etc/letsencrypt \
-v /docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt \
-v /docker/letsencrypt-docker-nginx/src/letsencrypt/letsencrypt-site:/data/letsencrypt \
-v "/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" \
certbot/certbot \
certonly --webroot \
--register-unsafely-without-email --agree-tos \
--webroot-path=/data/letsencrypt \
--staging \
-d everyonecancontribute.com -d www.everyonecancontribute.com
```

```
sudo docker run --rm -it --name certbot \
-v /docker-volumes/etc/letsencrypt:/etc/letsencrypt \
-v /docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt \
-v /docker/letsencrypt-docker-nginx/src/letsencrypt/letsencrypt-site:/data/letsencrypt \
certbot/certbot \
--staging \
certificates
```

Cleanup.
```
rm -rf /docker-volumes/
```

### Production certificate

```
docker run -it --rm \
-v /docker-volumes/etc/letsencrypt:/etc/letsencrypt \
-v /docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt \
-v /docker/letsencrypt-docker-nginx/src/letsencrypt/letsencrypt-site:/data/letsencrypt \
-v "/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" \
certbot/certbot \
certonly --webroot \
--email michael.friedrich@gmail.com --agree-tos --no-eff-email \
--webroot-path=/data/letsencrypt \
-d everyonecancontribute.com -d www.everyonecancontribute.com
```



```
cd /docker/letsencrypt-docker-nginx/src/letsencrypt
docker-compose down
```

```
mkdir -p /docker/letsencrypt-docker-nginx/src/production/production-site
mkdir -p /docker/letsencrypt-docker-nginx/src/production/dh-param
```

```
vim /docker/letsencrypt-docker-nginx/src/production/docker-compose.yml

version: '3.1'

services:

  production-nginx-container:
    container_name: 'production-nginx-container'
    image: nginx:latest
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - ./production.conf:/etc/nginx/conf.d/default.conf
      - ./production-site:/usr/share/nginx/html
      - ./dh-param/dhparam-2048.pem:/etc/ssl/certs/dhparam-2048.pem
      - /docker-volumes/etc/letsencrypt/live/everyonecancontribute.com/fullchain.pem:/etc/letsencrypt/live/everyonecancontribute.com/fullchain.pem
      - /docker-volumes/etc/letsencrypt/live/everyonecancontribute.com/privkey.pem:/etc/letsencrypt/live/everyonecancontribute.com/privkey.pem
    networks:
      - docker-network

networks:
  docker-network:
    driver: bridge
```

```
vim /docker/letsencrypt-docker-nginx/src/production/production.conf

server {
    listen      80;
    listen [::]:80;
    server_name ohhaieveryonecancontributethere.com www.everyonecancontribute.com;

    location / {
        rewrite ^ https://$host$request_uri? permanent;
    }

    #for certbot challenges (renewal process)
    location ~ /.well-known/acme-challenge {
        allow all;
        root /data/letsencrypt;
    }
}

#https://everyonecancontribute.com
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name everyonecancontribute.com;

    server_tokens off;

    ssl_certificate /etc/letsencrypt/live/everyonecancontribute.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/everyonecancontribute.com/privkey.pem;

    ssl_buffer_size 8k;

    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;

    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8;

    return 301 https://www.everyonecancontribute.com$request_uri;
}

#https://www.everyonecancontribute.com
server {
    server_name www.everyonecancontribute.com;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_tokens off;

    ssl on;

    ssl_buffer_size 8k;
    ssl_dhparam /etc/ssl/certs/dhparam-2048.pem;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
    ssl_prefer_server_ciphers on;
    ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

    ssl_ecdh_curve secp384r1;
    ssl_session_tickets off;

    # OCSP stapling
    ssl_stapling on;
    ssl_stapling_verify on;
    resolver 8.8.8.8 8.8.4.4;

    ssl_certificate /etc/letsencrypt/live/everyonecancontribute.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/everyonecancontribute.com/privkey.pem;

    root /usr/share/nginx/html;
    index index.html;
}
```

```
openssl dhparam -out /docker/letsencrypt-docker-nginx/src/production/dh-param/dhparam-2048.pem 2048
```

```
vim /docker/letsencrypt-docker-nginx/src/production/production-site/index.html
```

```
cd /docker/letsencrypt-docker-nginx/src/production
docker-compose up -d
```

### letsencrypt renew

```
vim /docker/letsencrypt-docker-nginx/src/production/docker-compose.yml

production-nginx-container:
    container_name: 'production-nginx-container'
    image: nginx:latest
    ports:
      - "80:80"
      - "443:443"
    volumes:
      #other mapped volumes...
      #for certbot challenges
      - /docker-volumes/data/letsencrypt:/data/letsencrypt
    networks:
      - docker-network

```

```
crontab -e
0 23 * * * docker run --rm -it --name certbot -v "/docker-volumes/etc/letsencrypt:/etc/letsencrypt" -v "/docker-volumes/var/lib/letsencrypt:/var/lib/letsencrypt" -v "/docker-volumes/data/letsencrypt:/data/letsencrypt" -v "/docker-volumes/var/log/letsencrypt:/var/log/letsencrypt" certbot/certbot renew --webroot -w /data/letsencrypt --quiet && docker kill --signal=HUP production-nginx-container
```

## Monitoring

### Prometheus Node Exporter

```
apt-get -y install prometheus-node-exporter
systemctl enable prometheus-node-exporter
systemctl start prometheus-node-exporter
```

Modify the default ignored filesystems as otherwise no metrics are collected.

```
vim /etc/default/prometheus-node-exporter

ARGS="--collector.diskstats.ignored-devices=^(ram|loop|fd|(h|s|v|xv)d[a-z]|nvme\\d+n\\d+p)\\d+$ \
      --collector.filesystem.ignored-mount-points=^/(run|dev|proc|sys|var/lib/docker/.+)($|/) \
      --collector.netdev.ignored-devices=^lo$ \
      --collector.textfile.directory=/var/lib/prometheus/node-exporter"
```

### Docker Metrics

```
vim /etc/docker/daemon.json

{
  "metrics-addr" : "0.0.0.0:9323",
  "experimental" : true
}

systemctl restart docker

curl everyonecancontribute:9223/metrics
```

### TLS Certificate Checks

https://github.com/ribbybibby/ssl_exporter

```
cd /docker/prometheus
docker-compose up -d

curl everyonecancontribute.com:9219/probe?target=everyonecancontribute.com:443
```


